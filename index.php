<html>
    <head>
        <link rel="stylesheet" href="./css/main.css"/>
        <title>WorkingConcept / DyingConcept</title>
        <link rel="shortcut icon" type="image/png" href="./img/logo.jpeg"/>
    </head>
    <body>


    <header>
        <!-- Carousel
================================================== -->
            <div id="myCarousel" class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                    <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                    <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item">
                        <img src="./img/pouf1.png" alt="lol">
                    </div>
                    <div class="item">
                        <img src="./img/pouf2.jpg" alt="lol">
                    </div>
                    <div class="item">
                        <img src="./img/pouf3.jpg" alt="lol">
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
			</div>
			<!-- /.carousel -->
        <div id="menu"> <!-- Menu -->
            <p><?php echo $notif;?></p>
            <div id="top_box">
                <p><a class="login" href="./login.html">login/register</a></p>
            </div>
			<div id="top">
				<div id="titre">
					<div>
						<a href="./index.php"><img id="logo" src="./img/logo.png" alt="logo"/></a>
					</div>
                    <div id="icones">
                        <img src="./img/linkedin-logo.png" alt="linkdin-logo" />
                        <img src="./img/instagram-logo.png" alt="instagram-logo" />
                        <img src="./img/pinterest_logo.png" alt="pinterest-logo" />
                        <img src="./img/twitter-logo.png" alt="twitter-logo" />
                        <img src="./img/Facebookf.png" alt="facebook-logo" />
                    </div>
                    <div id="bandeau">
                    </div>
                    <div id="onglet">
                        <nav>
                            <ul>
                                <li class="ombre"><a class="items" href="./src/contact.php">Contact</a></li>
                                <li class="ombre"><a class="items" href="./src/about.php">About</a></li>
                                <li class="ombre"><a class="items" href="./src/submitYourConcept.php">Submit Your Concept</a></li>
                                <li class="ombre" onmouseover="affiche('cat_villes');" onmouseout="cache('cat_villes');"><a class="items" href="./src/article-list" onmouseover="affiche('cat_villes');" onmouseout="cache('cat_villes');">Categories / Villes</a>
                                    <ul id="cat_villes" onmouseover="affiche('cat/villes');" onmouseout="cache('cat_villes');">
                                        <div id="button">
                                            <input type="button" name="OK" value="Rechercher des Articles" onclick="self.location.href='./vue/liste.php?categorie=resto&ville=paris'" style="width: 288px; height: 20px">
                                        </div>

                                        <div class="transparent">
                                            <div id="menu-column-1">
                                                <!-- VILLES -->
                                                <li class="ville">
                                                    <input type="checkbox" name="Paris" value="paris" /> Paris
                                                </li>
                                                <li class="ville">
                                                    <input type="checkbox" name="Paris" value="paris" /> Paris
                                                </li>
                                                <li class="ville">
                                                     <input type="checkbox" name="Paris" value="paris" /> Paris
                                                </li>
                                                <li class="ville">
                                                    <input type="checkbox" name="Paris" value="paris" /> Paris
                                                </li>
                                                <li class="ville">
                                                    <input type="checkbox" name="Paris" value="paris" /> Paris
                                                </li>
                                            </div>

                                            <div id="menu-column-2">
                                                <!-- CATEGORIES -->
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                                <li class="cat">
                                                    <input type="checkbox" name="Restaurant" value="resto" /> Restaurant
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <!-- <li> <a class="menu" href="./index.php">Home</a> </li> -->
                            </ul>
					    </nav>
					</div>
				</div>
			</div>
        </div>
	</header>


    <?php
        $i = 0;
        while ($i < 500) {
            echo "<br />";
            $i++;
        }
    ?>
    <script type="text/javascript">
        function affiche(id) {
            document.getElementById(id).style.display = "block";
        }
        function cache(id) {
            document.getElementById(id).style.display = "none";
        }
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
        $(function(){
            $(window).scroll(
                function () {
                    if ($(this).scrollTop() > 526) {
                        $('#menu').addClass("fixMenu");
                        $('#top').addClass("fixLogo");

                    } else {
                        $('#menu').removeClass("fixMenu");
                        $('#top').removeClass("fixLogo");

                    }
                }
            );
        });
    </script>

    <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
    <script src="./js/bootstrap.min.js"></script>

    <script>
        (function ($) {
            //slider random
            $('.item').eq(Math.floor((Math.random() * $('.item').length))).addClass("active");

            // parallax
            // Cache the Window object
            $window = $(window);

            $('section[data-type="background"]').each(function(){
                var $bgobj = $(this); // assigning the object

                $(window).scroll(function() {

                    // Scroll the background at var speed
                    // the yPos is a negative value because we're scrolling it UP!
                    var yPos = -($window.scrollTop() / $bgobj.data('speed'));

                    // Put together our final background position
                    var coords = '50% '+ yPos + 'px';

                    // Move the background
                    $bgobj.css({ backgroundPosition: coords });

                }); // window scroll Ends

            });

        })(jQuery);

        /*
         * Create HTML5 elements for IE's sake
         */

        document.createElement("article");
        document.createElement("section");
    </script>
    </body>
</html>
