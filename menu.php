<html>

	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="./vue/index.css" />
		<title>retro.fr</title>
		<link rel="shortcut icon" type="image/png" href="./vue/favicon.png"/>
	</head>
	<header>
	<div id="first_topbox">
	<p><a class="login" href="./vue/login.html">login/registrer</a></p>
	</div>
	<div id=topbox>
		<div id="titre_principal">
			<div id="logo">
				<a href="./index.php"><img src="./vue/images/logo.png" alt="Logo"/></a>
			</div>
		</div>
		<nav>
		<ul>
			<li><a class="menu" href="./index.php">Home</a></li>
			<li><a class="categorie">Console</a>
				<ul>
				<li><a href="./vue/console.php?console=Xbox">Xbox</a></li>
				<li><a href="./vue/console.php?console=PS">PS</a></li>
				<li><a href="./vue/console.php?console=SuperNES">SuperNES</a></li>
				<li><a href="./vue/console.php?console=Nintendo 64">Nintendo 64</a></li>
				<li><a href="./vue/console.php?console=PC">PC</a></li>
				<li><a href="./vue/console.php?console=PS2">PS2</a></li>
				<li><a href="./vue/console.php?console=NES">NES</a></li>
				<li><a href="./vue/console.php?console=Dreamcast">Dreamcast</a></li>
				<li><a href="./vue/console.php?console=Gamecube">Gamecube</a></li>
				<li><a href="./vue/console.php?console=Gameboy">Gameboy</a></li>
				</ul>
			</li>
			<li><a class="categorie">Categories</a>
			<ul>
				<li><a href="./vue/categorie.php?categorie=action">Action</a></li>
				<li><a href="./vue/categorie.php?categorie=combat">Combat</a></li>
				<li><a href="./vue/categorie.php?categorie=course">Course</a></li>
				<li><a href="./vue/categorie.php?categorie=sport">Sport</a></li>
				<li><a href="./vue/categorie.php?categorie=autre">Autre</a></li>
				<li><a href="./vue/categorie.php?categorie=FPS">FPS</a></li>
				<li><a href="./vue/categorie.php?categorie=plateforme">Platform</a></li>
				<li><a href="./vue/categorie.php?categorie=RPG">Role</a></li>
				<li><a href="./vue/categorie.php?categorie=gestion">Gestion</a></li>
			</ul>
			</li>
			<li><a class="menu" href="./vue/product.html">Produit</a></li>
			<li><a class="menu" href="./vue/contact.html">Contact</a></li>
		</ul>
		<a href="./vue/user.html"><img class="user" src="./vue/images/user.png" alt="user"/></a>
		<a href="./vue/panier.php"><img class="panier" src="./vue/images/panier.png" alt="panier"/></a>
		</nav>
	</div>
	</header>
	<body>
		<div id="banniere">
			<div class="best_retro">
				<p> Best retro games </p></div>
			<div class="best_price">
				<p> Best price </p></div>
			<div class="allgame">
				<p><a href="#">See all games </a></p></div>
		</div>
		<div id="body">
			<div class="bloc">

			</div>
		</div>
		<hr>
	</body>
	<footer>
	<p id="copyright_text">
	&copy; tcouet, qmoinat : 2016
	</footer>
</html>
